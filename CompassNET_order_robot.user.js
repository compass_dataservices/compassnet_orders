// ==UserScript==
// @name         CompassNET Order Robot
// @namespace    https://compass-usa.com
// @version      1.1.5
// @updateURL    https://gist.github.com/WisdomWolf/b3dc6cf484f0dab1167e89bac6993b1b/raw/CompassNET_order_robot.user.js
// @description  Automate CompassNET Order form
// @author       WisdomWolf
// @match        https://remedyweb.compass-usa.com/arsys/forms/remedyprod/OM%3AOrderDrafts/Support/*
// @require      https://craig.global.ssl.fastly.net/js/mousetrap/mousetrap.min.js
// @grant        none
// ==/UserScript==
/* jshint -W097 */
'use strict';

var unitData;

var targetInstallDate = $('#arid_WIN_0_777900016');
var earliestDeliveryDate = $('#arid_WIN_0_777900122');

var posCreditCardYes = $('#WIN_0_rc0id777900066'); //attr('checked', [true | false]) to select
var posCreditCardNo = $('#WIN_0_rc1id777900066');

var kioskSystemYes = $('#WIN_0_rc0id777900097');
var kioskSystemNo = $('#WIN_0_rc1id777900097');

var cbordYes = $('#WIN_0_rc0id777900067');
var cbordNo = $('#WIN_0_rc1id777900067');

var nonCompassPCYes = $('#WIN_0_rc0id777900069');
var nonCompassPCNo = $('#WIN_0_rc1id777900069');

var dvrSystemYes = $('#WIN_0_rc0id777900070');
var dvrSystemNo = $('#WIN_0_rc1id777900070');

var teleWorkerYes = $('#WIN_0_rc0id777900068');
var teleWorkerNo = $('#WIN_0_rc1id777900068');

var p2peDevicesYes = $('#WIN_0_rc0id777910053');
var p2peDevicesNo = $('#WIN_0_rc1id777910053');

var tpgServerYes = $('#WIN_0_rc0id777900302');
var tpgServerNo = $('#WIN_0_rc1id777900302');

var fundingStationsYes = $('#WIN_0_rc0id777900303');
var fundingStationsNo = $('#WIN_0_rc1id777900303');

var digitalSignageYes = $('#WIN_0_rc0id777900304');
var digitalSignageNo = $('#WIN_0_rc1id777900304');

var addContactButton = $('#WIN_0_777900031');

var primaryContactName = $('#arid_WIN_0_1000005068');
var primaryContactPhone = $('#arid_WIN_0_1000005069');
var unitEmail = $('#arid_WIN_0_1000005071');

var btnOrderDetail = $('#WIN_0_777900113');  //add handler to this
var btnSubmitOrder = $('#WIN_0_777900100');

//-------------------------------------------//

var costCenterNumber = $('#arid_WIN_0_1000005060');
var posType = $('#arid_WIN_0_777900051');
var totalCCTerminals = $('#arid_WIN_0_777910029');
var p2peDeviceTotal = $('#arid_WIN_0_777910054');
var p2peModel = $('#arid_WIN_0_777910055');

var ispProvider = $('#arid_WIN_0_777900022');
var dhcp = $('#arid_WIN_0_777910030');
var firstIP = $('#arid_WIN_0_777900036');
var publicGateway = $('#arid_WIN_0_777900027');
var lastIP = $('#arid_WIN_0_777900025');
var subnetMask = $('#arid_WIN_0_777900026');
var primaryDNS = $('#arid_WIN_0_777900028');
var secondaryDNS = $('#arid_WIN_0_777900029');

$('input').addClass('mousetrap');
localStorage.setItem('contactRunCount', 0);
localStorage.setItem('compass_unit_number', null);

//todo figure out how to trigger change population

function updateField(element, value) {
    element.val(value);
    triggerChange(element);
}

function triggerChange(element) {
    if (element.length != undefined) {
        element = element[0];
    }
    if ("createEvent" in document) {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("change", false, true);
        element.dispatchEvent(evt);
    }
    else {
        element.fireEvent("onchange");
    }
    return element;
}

costCenterNumber.bind('change', function() {
    unitDataRaw = $.getJSON('https://tpa.compassfss.com/get_json/' + $(this).val());
    posCreditCardYes.click();
    kioskSystemNo.click();
    cbordNo.click();
    nonCompassPCNo.click();
    dvrSystemNo.click();
    teleWorkerNo.click();
    p2peDevicesYes.click();
    tpgServerNo.click();
    fundingStationsNo.click();
    digitalSignageNo.click();
    updateField(targetInstallDate,earliestDeliveryDate.val());
    
    setTimeout(fillFields, 1000);
});

function countTerms(device_array) {
  var qty = 0;
  $.each(device_array, function(index, value) {
    qty += value.qty;
  });
  return qty;
}

function fillFields() {
    unitData = $.parseJSON(unitDataRaw.responseText);
    
    updateField(primaryContactName, unitData.unit_contact.name);
    updateField(primaryContactPhone, unitData.unit_contact.phone);
    updateField(unitEmail, unitData.unit_contact.email);
    localStorage.setItem('compass_unit_number', costCenterNumber.val());
}

function fillNetworkInfo() {
    console.log('filling network info');
    if (unitData == undefined) {
        console.log('getting fresh unit data');
        unitData = $.parseJson($.getJSON('https://tpa.compassfss.com/get_json/' + costCenterNumber.val()));
    }
    
    var posVendor;
    if (unitData.network_devices.pos_terms[0].type == 'simphony') {
        posVendor = 'MICROS';
    } else {
        posVendor = 'AGILYSYS';
    }
    
    updateField(posType, posVendor);
    
    var numTerminals = countTerms(unitData.network_devices.pos_terms);
    updateField(totalCCTerminals, numTerminals);
    
    updateField(ispProvider, unitData.service_provider);
    console.log('updating ISP to: ' + unitData.service_provider);
    updateField(firstIP, unitData.first_ip);
    console.log('updating first IP to: ' + unitData.first_ip);
    updateField(publicGateway, unitData.gateway_ip);
    updateField(lastIP, unitData.last_ip);
    updateField(subnetMask, unitData.subnet_mask);
    updateField(primaryDNS, unitData.dns_1);
    updateField(secondaryDNS, unitData.dns_2);
}

btnOrderDetail.bind('click', function() {
    setTimeout(fillNetworkInfo, 1000);
});

Mousetrap.bind('alt+u', function() {
	//alert('Updating Dashboard via UserScript');
	fillNetworkInfo();
});